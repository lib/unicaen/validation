<?php

namespace UnicaenValidation\View\Helper;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenValidation\Service\ValidationType\ValidationTypeService;

class ValidationValiderViewHelperFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationValiderViewHelper
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ValidationValiderViewHelper
    {
        /** @var ValidationTypeService $validationTypeService */
        $validationTypeService = $container->get(ValidationTypeService::class);

        $helper = new ValidationValiderViewHelper();
        $helper->setValidationTypeService($validationTypeService);
        return $helper;
    }
}