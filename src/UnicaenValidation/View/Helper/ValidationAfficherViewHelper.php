<?php

namespace UnicaenValidation\View\Helper;

use UnicaenValidation\Entity\Db\ValidationInstance;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;

class ValidationAfficherViewHelper extends AbstractHelper
{
    /**
     * @param ValidationInstance $validation
     * @param array $options
     * @desc Options possible :
     * 'afficher-code' default false : affiche le code du type de validation
     * 'afficher-type' default true : affiche le libellé du type de validation
     * 'afficher-justification' default true : affiche la justification
     * @return string|Partial
     */
    public function __invoke(ValidationInstance $validation, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('validation-afficher', ['instance' => $validation, 'options' => $options]);
    }
}