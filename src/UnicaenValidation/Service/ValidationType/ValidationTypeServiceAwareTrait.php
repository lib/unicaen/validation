<?php

namespace UnicaenValidation\Service\ValidationType;

trait ValidationTypeServiceAwareTrait {

    private ValidationTypeService $validationTypeService;

    /**
     * @return ValidationTypeService
     */
    public function getValidationTypeService() : ValidationTypeService
    {
        return $this->validationTypeService;
    }

    /**
     * @param ValidationTypeService $validationTypeService
     */
    public function setValidationTypeService(ValidationTypeService $validationTypeService) : void
    {
        $this->validationTypeService = $validationTypeService;
    }
}