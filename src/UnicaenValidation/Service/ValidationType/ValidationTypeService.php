<?php

namespace UnicaenValidation\Service\ValidationType;

use Doctrine\ORM\Exception\NotSupported;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenValidation\Entity\Db\ValidationType;

class ValidationTypeService {
    use EntityManagerAwareTrait;

    /** GESTION DES ENTITES *******************************************************************************************/

    /**
     * @param ValidationType $type
     * @return ValidationType
     */
    public function create(ValidationType $type) : ValidationType
    {
        try {
            $this->getEntityManager()->persist($type);
            $this->getEntityManager()->flush($type);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $type;
    }

    /**
     * @param ValidationType $type
     * @return ValidationType
     */
    public function update(ValidationType $type) : ValidationType
    {
        try {
            $this->getEntityManager()->flush($type);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $type;
    }

    /**
     * @param ValidationType $type
     * @return ValidationType
     */
    public function delete(ValidationType $type) : ValidationType
    {
        try {
            $this->getEntityManager()->remove($type);
            $this->getEntityManager()->flush($type);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $type;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        try {
            $qb = $this->getEntityManager()->getRepository(ValidationType::class)->createQueryBuilder('vtype');
        } catch (NotSupported $e) {
            throw new RuntimeException("Un problème est survenu lors de la création du QueryBuilder de [".ValidationType::class."]",0,$e);
        }

        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return ValidationType[]
     */
    public function getValidationsTypes(string $champ = "libelle", string $ordre = "ASC") : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('vtype.' . $champ, $ordre)
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return array
     */
    public function getValidationsTypesAsOptions(string $champ = "libelle", string $ordre = "ASC") : array
    {
        $result = $this->getValidationsTypes($champ, $ordre);
        $array = [];
        foreach ($result as $item) $array[$item->getId()] = $item->getCode();
        return $array;
    }

    /**
     * @param int|null $id
     * @return ValidationType|null
     */
    public function getValidationType(?int $id) : ?ValidationType
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('vtype.id = :id')
            ->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs ValidationType partagent le même id [".$id."]", 0, $e);
        }

        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return ValidationType|null
     */
    public function getRequestedValidationType(AbstractActionController $controller, string $param='type') : ?ValidationType
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getValidationType($id);
        return $result;
    }

    /**
     * @param string $code
     * @return ValidationType|null
     */
    public function getValidationTypeByCode(string $code) : ?ValidationType
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('vtype.code = :code')
            ->setParameter('code', $code)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs ValidationType partagent le même code [".$code."]", 0, $e);
        }
        return $result;
    }
}