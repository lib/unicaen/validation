<?php

namespace UnicaenValidation\Service\ValidationInstance;

trait ValidationInstanceServiceAwareTrait {

    private ValidationInstanceService $validationInstanceService;

    /**
     * @return ValidationInstanceService
     */
    public function getValidationInstanceService() : ValidationInstanceService
    {
        return $this->validationInstanceService;
    }

    /**
     * @param ValidationInstanceService $validationInstanceService
     */
    public function setValidationInstanceService(ValidationInstanceService $validationInstanceService) : void
    {
        $this->validationInstanceService = $validationInstanceService;
    }
}