<?php

namespace UnicaenValidation\Service\ValidationInstance;

use Doctrine\ORM\Exception\NotSupported;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenValidation\Entity\Db\ValidationInstance;
use UnicaenValidation\Entity\HasValidationsInterface;
use UnicaenValidation\Service\ValidationType\ValidationTypeServiceAwareTrait;

class ValidationInstanceService {
    use EntityManagerAwareTrait;
    use ValidationTypeServiceAwareTrait;

    /** GESTION DES ENTITES *******************************************************************************************/

    /**
     * @param ValidationInstance $instance
     * @return ValidationInstance
     */
    public function create(ValidationInstance $instance) : ValidationInstance
    {
        try {
            $this->getEntityManager()->persist($instance);
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $instance;
    }

    /**
     * @param ValidationInstance $instance
     * @return ValidationInstance
     */
    public function update(ValidationInstance $instance) : ValidationInstance
    {
        try {
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $instance;
    }

    /**
     * @param ValidationInstance $instance
     * @return ValidationInstance
     */
    public function historise(ValidationInstance $instance) : ValidationInstance
    {
        try {
            $instance->historiser();
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $instance;
    }

    /**
     * @param ValidationInstance $instance
     * @return ValidationInstance
     */
    public function restore(ValidationInstance $instance) : ValidationInstance
    {

        try {
            $instance->dehistoriser();
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $instance;
    }

    /**
     * @param ValidationInstance $instance
     * @return ValidationInstance
     */
    public function delete(ValidationInstance $instance) : ValidationInstance
    {
        try {
            $this->getEntityManager()->remove($instance);
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.",0,$e);
        }

        return $instance;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        try {
            $qb = $this->getEntityManager()->getRepository(ValidationInstance::class)->createQueryBuilder('vinstance')
                ->addSelect('createur')->join('vinstance.histoCreateur', 'createur')
                ->addSelect('modificateur')->join('vinstance.histoModificateur', 'modificateur')
                ->addSelect('vtype')->join('vinstance.type', 'vtype');
        } catch (NotSupported $e) {
            throw new RuntimeException("Un problème est survenu lors de la création du QueryBuilder [".ValidationInstance::class."]",0,$e);
        }
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return ValidationInstance[]
     */
    public function getValidationsInstances(string $champ = 'histoModification', string $ordre = 'DESC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('vinstance.' . $champ, $ordre)
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param int|null $id
     * @return ValidationInstance|null
     */
    public function getValidationInstance(?int $id) : ?ValidationInstance
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('vinstance.id = :id')
            ->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs ValidationInstance partagent le même id [".$id."]", 0, $e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return ValidationInstance|null
     */
    public function getRequestedValidationInstance(AbstractActionController $controller, string $param = "validation") : ?ValidationInstance
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getValidationInstance($id);
        return $result;
    }

    /**
     * @param ?string $term
     * @return User[]
     */
    public function findValidateurByTerm(?string $term) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere("LOWER(createur.displayName) like :search")
            ->setParameter('search', '%'.strtolower($term).'%');
        $result = $qb->getQuery()->getResult();

        $validateurs = [];
        /** @var  $item */
        foreach ($result as $item) {
            $validateurs[$item->getHistoCreateur()->getId()] = $item;
        }
        return $validateurs;
    }

    /**
     * @param array $params
     * @return ValidationInstance[]
     */
    public function getValidationsInstancesWithFiltre(array $params) : array
    {
        $qb = $this->createQueryBuilder();
        if (isset($params['reponse']) AND $params['reponse'] !== '') {
            $qb = $qb->andWhere('vinstance.refus = :reponse')->setParameter('reponse', $params['reponse']);
        }
        if (isset($params['type'])  AND $params['type'] !== '') {
            $qb = $qb->andWhere('vtype.code = :code')->setParameter('code', $params['type']);
        }
        if (isset($params['validateur']) AND isset($params['validateur']['id']) AND $params['validateur']['id'] !== '') {
            $id = ((int) $params['validateur']['id']);
            $qb = $qb->andWhere('createur.id = :id')->setParameter('id', $id);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FACADE ********************************************************************************************************/

    /**
     * @param string $code
     * @param string|null $valeur
     * @param string|null $justification
     * @return ValidationInstance
     */
    public function createWithCode(string $code, bool $refus = false, ?string $justification = null) : ValidationInstance
    {
        $type = $this->getValidationTypeService()->getValidationTypeByCode($code);

        $instance = new ValidationInstance();
        $instance->setType($type);
        $instance->setRefus($refus);
        $instance->setJustification($justification);
        $this->create($instance);

        return $instance;
    }

    public function setValidationActive(HasValidationsInterface $element, string $code, bool $refus = false, ?string $justification = null) : ValidationInstance
    {
        //historisation de l'état actif actuel (s'il existe)
        $validationActive = $element->getValidationActiveByTypeCode($code);
        if (isset($validationActive)) {
            $this->historise($validationActive);
        }

        //creation de la nouvelle instance
        $validation = $this->createWithCode($code, $refus,  $justification);

        //ajout à l'element et persist
        $element->addValidation($validation);
        try {
            $this->getEntityManager()->persist($element);
            $this->getEntityManager()->flush($element);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de l'ajout de  actif de [".get_class($element)."]",0,$e);
        }
        return $validation;
    }
}