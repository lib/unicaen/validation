<?php

namespace UnicaenValidation\Service\ValidationInstance;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenValidation\Service\ValidationType\ValidationTypeService;

class ValidationInstanceServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationInstanceService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ValidationInstanceService
    {
        /**
         * @var EntityManager $entityManager
         * @var ValidationTypeService $validationTypeService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $validationTypeService = $container->get(ValidationTypeService::class);

        $service = new ValidationInstanceService();
        $service->setEntityManager($entityManager);
        $service->setValidationTypeService($validationTypeService);
        return $service;
    }
}