<?php

namespace UnicaenValidation\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenValidation\Form\ValidationType\ValidationTypeForm;
use UnicaenValidation\Service\ValidationType\ValidationTypeService;

class ValidationTypeControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationTypeController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ValidationTypeController
    {
        /**
         * @var ValidationTypeService $validationTypeService
         */
        $validationTypeService = $container->get(ValidationTypeService::class);

        /**
         * @var ValidationTypeForm $validationTypeForm
         */
        $validationTypeForm = $container->get('FormElementManager')->get(ValidationTypeForm::class);

        $controller = new ValidationTypeController();
        $controller->setValidationTypeService($validationTypeService);
        $controller->setValidationTypeForm($validationTypeForm);
        return $controller;
    }
}