<?php

namespace UnicaenValidation\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenValidation\Form\ValidationInstance\ValidationInstanceForm;
use UnicaenValidation\Service\ValidationInstance\ValidationInstanceService;
use UnicaenValidation\Service\ValidationType\ValidationTypeService;

class ValidationInstanceControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationInstanceController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ValidationInstanceController
    {
        /**
         * @var ValidationInstanceService $validationInstanceService
         * @var ValidationTypeService $validationTypeService
         */
        $validationInstanceService = $container->get(ValidationInstanceService::class);
        $validationTypeService = $container->get(ValidationTypeService::class);

        /**
         * @var ValidationInstanceForm $validationInstanceForm
         */
        $validationInstanceForm = $container->get('FormElementManager')->get(ValidationInstanceForm::class);

        $controller = new ValidationInstanceController();
        $controller->setValidationInstanceService($validationInstanceService);
        $controller->setValidationInstanceForm($validationInstanceForm);
        $controller->setValidationTypeService($validationTypeService);
        return $controller;
    }
}