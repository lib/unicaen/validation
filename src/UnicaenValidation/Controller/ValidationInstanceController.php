<?php

namespace UnicaenValidation\Controller;

use Laminas\Http\Response;
use Laminas\View\Model\JsonModel;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenValidation\Entity\Db\ValidationInstance;
use UnicaenValidation\Form\ValidationInstance\ValidationInstanceFormAwareTrait;
use UnicaenValidation\Service\ValidationInstance\ValidationInstanceServiceAwareTrait;
use UnicaenValidation\Service\ValidationType\ValidationTypeServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class ValidationInstanceController extends AbstractActionController {
    use ValidationInstanceServiceAwareTrait;
    use ValidationTypeServiceAwareTrait;
    use ValidationInstanceFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $params = $this->params()->fromQuery();

        $instances = $this->getValidationInstanceService()->getValidationsInstancesWithFiltre($params);
        $types = $this->getValidationTypeService()->getValidationsTypes();

        return new ViewModel([
            'instances' => $instances,
            'types' => $types,
            'params' => $params,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $instance = new ValidationInstance();
        $form = $this->getValidationInstancForm();
        $form->setAttribute('action', $this->url()->fromRoute('validation/instance/ajouter', [], [], true));
        $form->bind($instance);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getValidationInstanceService()->create($instance);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-validation/default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'un type de validation",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $instance = $this->getValidationInstanceService()->getRequestedValidationInstance($this);
        $form = $this->getValidationInstancForm();
        $form->setAttribute('action', $this->url()->fromRoute('validation/instance/modifier', ['validation' => $instance->getId()], [], true));
        $form->bind($instance);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getValidationInstanceService()->update($instance);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-validation/default/default-form');
        $vm->setVariables([
            'title' => "Modification d'un type de validation",
            'form' => $form,
        ]);
        return $vm;
    }

    public function historiserAction() : Response
    {
        $instance = $this->getValidationInstanceService()->getRequestedValidationInstance($this);
        $this->getValidationInstanceService()->historise($instance);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('validation/instance', [], ["query" => $this->params()->fromQuery()], true);
    }

    public function restaurerAction() : Response
    {
        $instance = $this->getValidationInstanceService()->getRequestedValidationInstance($this);
        $this->getValidationInstanceService()->restore($instance);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('validation/instance', [], ["query" => $this->params()->fromQuery()], true);
    }

    public function detruireAction() : ViewModel
    {
        $instance = $this->getValidationInstanceService()->getRequestedValidationInstance($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getValidationInstanceService()->delete($instance);
            exit();
        }

        $vm = new ViewModel();
        if ($instance !== null) {
            $vm->setTemplate('unicaen-validation/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la validation ",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('validation/instance/detruire', ["validation" => $instance->getId()], [], true),
            ]);
        }
        return $vm;
    }

    public function rechercherValidateurAction()
    {
        if (($term = $this->params()->fromQuery('term'))) {
            /** @var User $validateurs */
            $validateurs = $this->getValidationInstanceService()->findValidateurByTerm($term);
            foreach ($validateurs as $validateur) {
                $result[] = array(
                    'id' => $validateur->getHistoCreateur()->getId(),
                    'label' => $validateur->getHistoCreateur()->getDisplayName(),
                );
            }
            usort($result, function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });
            return new JsonModel($result);
        }
        exit;
    }
}
