<?php

namespace UnicaenValidation\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ValidationinstancePrivileges extends Privileges
{
    const VALIDATIONINSTANCE_AFFICHER = 'validationinstance-validationinstance_afficher';
    const VALIDATIONINSTANCE_MODIFIER = 'validationinstance-validationinstance_modifier';
    const VALIDATIONINSTANCE_HISTORISER = 'validationinstance-validationinstance_historiser';
    const VALIDATIONINSTANCE_DETRUIRE = 'validationinstance-validationinstance_detruire';
}