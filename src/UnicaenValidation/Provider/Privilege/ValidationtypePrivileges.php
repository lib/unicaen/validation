<?php

namespace UnicaenValidation\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ValidationtypePrivileges extends Privileges
{
    const VALIDATIONTYPE_AFFICHER   = 'validationtype-validationtype_afficher';
    const VALIDATIONTYPE_MODIFIER   = 'validationtype-validationtype_modifier';
    const VALIDATIONTYPE_DETRUIRE   = 'validationtype-validationtype_detruire';
}