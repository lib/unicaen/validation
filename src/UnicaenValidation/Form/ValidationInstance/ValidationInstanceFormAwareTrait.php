<?php

namespace UnicaenValidation\Form\ValidationInstance;

trait ValidationInstanceFormAwareTrait {

    private ValidationInstanceForm $validationInstanceForm;

    public function getValidationInstanceForm() : ValidationInstanceForm
    {
        return $this->validationInstanceForm;
    }

    public function setValidationInstanceForm(ValidationInstanceForm $validationInstanceForm) : void
    {
        $this->validationInstanceForm = $validationInstanceForm;
    }

}