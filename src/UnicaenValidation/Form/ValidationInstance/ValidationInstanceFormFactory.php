<?php

namespace UnicaenValidation\Form\ValidationInstance;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenValidation\Service\ValidationType\ValidationTypeService;

class ValidationInstanceFormFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationInstanceForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ValidationInstanceForm
    {
        /**
         * @var ValidationTypeService $validationTypeService
         */
        $validationTypeService = $container->get(ValidationTypeService::class);

        /** @var ValidationInstanceHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(ValidationInstanceHydrator::class);

        $form = new ValidationInstanceForm();
        $form->setValidationTypeService($validationTypeService);
        $form->setHydrator($hydrator);
        return $form;
    }
}