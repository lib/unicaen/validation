<?php

namespace UnicaenValidation\Form\ValidationInstance;

use UnicaenValidation\Entity\Db\ValidationInstance;
use UnicaenValidation\Service\ValidationType\ValidationTypeServiceAwareTrait;
use Laminas\Hydrator\HydratorInterface;

class ValidationInstanceHydrator implements HydratorInterface {
    use ValidationTypeServiceAwareTrait;

    /**
     * @param ValidationInstance $object
     * @return array
     */
    public function extract(object $object) : array
    {
        $data = [
            'type'          => ($object AND $object->getType())?$object->getType()->getId():null,
            'valeur'        => $object?$object->getJustification():null,
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param ValidationInstance $object
     * @return ValidationInstance
     */
    public function hydrate(array $data, object $object)
    {
        $type = $this->getValidationTypeService()->getValidationType($data['type']);
        $object->setType($type);
        $object->setJustification($data['valeur'] ?? null);
        $object->setRefus($data['valeur'] === null || $data['valeur'] === "");
        return $object;
    }
}