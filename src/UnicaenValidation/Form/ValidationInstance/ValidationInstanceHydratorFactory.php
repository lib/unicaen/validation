<?php

namespace UnicaenValidation\Form\ValidationInstance;

use Interop\Container\ContainerInterface;
use UnicaenValidation\Service\ValidationType\ValidationTypeService;

class ValidationInstanceHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationInstanceHydrator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ValidationInstanceHydrator
    {
        /**
         * @var ValidationTypeService $validationTypeService
         */
        $validationTypeService = $container->get(ValidationTypeService::class);

        $hydrator = new ValidationInstanceHydrator();
        $hydrator->setValidationTypeService($validationTypeService);
        return $hydrator;
    }
}