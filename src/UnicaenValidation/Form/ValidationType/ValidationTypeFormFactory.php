<?php

namespace UnicaenValidation\Form\ValidationType;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ValidationTypeFormFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationTypeForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : ValidationTypeForm
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /** @var ValidationTypeHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(ValidationTypeHydrator::class);

        $form = new ValidationTypeForm();
        $form->setEntityManager($entityManager);
        $form->setHydrator($hydrator);
        return $form;
    }
}