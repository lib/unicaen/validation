<?php

namespace UnicaenValidation\Form\ValidationType;

trait ValidationTypeFormAwareTrait {

    private ValidationTypeForm $validationTypeForm;

    /**
     * @return ValidationTypeForm
     */
    public function getValidationTypeForm() : ValidationTypeForm
    {
        return $this->validationTypeForm;
    }

    /**
     * @param ValidationTypeForm $validationTypeForm
     */
    public function setValidationTypeForm(ValidationTypeForm $validationTypeForm) : void
    {
        $this->validationTypeForm = $validationTypeForm;
    }
}