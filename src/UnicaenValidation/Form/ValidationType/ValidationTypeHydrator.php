<?php

namespace UnicaenValidation\Form\ValidationType;

use UnicaenValidation\Entity\Db\ValidationType;
use Laminas\Hydrator\HydratorInterface;

class ValidationTypeHydrator implements HydratorInterface {

    /**
     * @param ValidationType $object
     * @return array
     */
    public function extract(object $object) : array
    {
        $data = [
            "code" => $object->getCode(),
            "libelle" => $object->getLibelle(),
            "refusable" => $object->isRefusable(),
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param ValidationType $object
     * @return ValidationType
     */
    public function hydrate(array $data, object $object)
    {
        $object->setCode($data['code'] ?? null);
        $object->setLibelle($data['libelle'] ?? null);
        $object->setRefusable($data['refusable'] ?? null);
        return $object;
    }

}