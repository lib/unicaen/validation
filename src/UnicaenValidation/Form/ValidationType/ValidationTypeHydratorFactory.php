<?php

namespace UnicaenValidation\Form\ValidationType;

use Interop\Container\ContainerInterface;

class ValidationTypeHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return ValidationTypeHydrator
     */
    public function __invoke(ContainerInterface $container) : ValidationTypeHydrator
    {
        $hydrator = new ValidationTypeHydrator();
        return $hydrator;
    }
}