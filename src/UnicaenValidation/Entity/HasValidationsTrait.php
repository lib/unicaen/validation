<?php

namespace UnicaenValidation\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use UnicaenValidation\Entity\Db\ValidationInstance;

trait HasValidationsTrait {

    private ?Collection $validations = null ;

    /**
     * @return ValidationInstance[]
     */
    public function getValidations() : array
    {
        return $this->validations->toArray();
    }

    /**
     * @param ValidationInstance $validation
     * @return void
     */
    public function addValidation(ValidationInstance $validation) : void
    {
        if ($this->validations === null) $this->validations = new ArrayCollection();
        $this->validations->add($validation);
    }

    /**
     * @param ValidationInstance $validation
     * @return void
     */
    public function removeValidation(ValidationInstance $validation) : void
    {
        $this->validations->removeElement($validation);
    }

    /**
     * @param string $typeCode
     * @return array
     */
    public function getValidationsByTypeCode(string $typeCode) : array
    {
        $result = [];
        /** @var ValidationInstance $validation */
        foreach ($this->validations as $validation) {
            if ($validation->getType()->getCode() === $typeCode) $result[] = $validation;
        }
        return $result;
    }
    /**
     * @param string $typeCode
     * @return ValidationInstance|null
     */
    public function getValidationActiveByTypeCode(string $typeCode) : ?ValidationInstance
    {
        /** @var ValidationInstance $validation */
        foreach ($this->validations as $validation) {
            if ($validation->getType()->getCode() === $typeCode AND $validation->estNonHistorise()) return $validation;
        }
        return null;
    }
    static public function decorateWithValidations(QueryBuilder $qb, string $entityName,  array $validations = []) : QueryBuilder
    {
        $qb = $qb
            ->leftJoin($entityName . '.validations', 'validation')->addSelect('validation')
            ->leftJoin('validation.type', 'validationtype')->addSelect('validationtype')
            ->andWhere('validation.histoDestruction IS NULL')
        ;

        if (!empty($validations)) {
            $qb = $qb->andWhere('validation.type in (:etats)')
                ->setParameter('validations', $validations);
        }
        return $qb;
    }



}