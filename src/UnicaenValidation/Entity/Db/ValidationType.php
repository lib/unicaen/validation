<?php

namespace UnicaenValidation\Entity\Db;

class ValidationType {

    private ?int $id = null;
    private ?string $code = null;
    private ?string $libelle = null;
    private ?bool $refusable = null;

    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCode() : ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return ValidationType
     */
    public function setCode(string $code) : ValidationType
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLibelle() : ?string
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     * @return ValidationType
     */
    public function setLibelle(string $libelle) : ValidationType
    {
        $this->libelle = $libelle;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRefusable() : bool
    {
        return $this->refusable??false;
    }

    /**
     * @param bool $refusable
     * @return ValidationType
     */
    public function setRefusable(bool $refusable) : ValidationType
    {
        $this->refusable = $refusable;
        return $this;
    }
}