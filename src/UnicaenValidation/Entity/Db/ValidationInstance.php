<?php

namespace UnicaenValidation\Entity\Db;

use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class ValidationInstance implements HistoriqueAwareInterface {
    use HistoriqueAwareTrait;

    private ?int $id = null;
    private ?ValidationType $type = null;
    private bool $refus = false;
    private ?string $justification = null;
    
    public function getId() : ?int
    {
        return $this->id;
    }

    public function getType() : ?ValidationType
    {
        return $this->type;
    }

    public function setType(ValidationType $type) : ValidationInstance
    {
        $this->type = $type;
        return $this;
    }

    public function isRefus(): bool
    {
        return $this->refus;
    }

    public function setRefus(bool $refus): void
    {
        $this->refus = $refus;
    }

    public function getJustification(): ?string
    {
        return $this->justification;
    }

    public function setJustification(?string $justification): void
    {
        $this->justification = $justification;
    }

    public function generateTooltipText() : string
    {
        $text  = "Validation effectuée<br/>";
        $text .= "par <span class='user'>".$this->histoModificateur->getDisplayName()."</span><br/>";
        $text .= "le <span class='date'>".$this->getHistoModification()->format('d/m/Y')."</span>";
        return $text;
    }

    /** toString functions */

    public function toStringDate() : string
    {
        return $this->getHistoCreation()->format('d/m/Y à H:i');
    }

    public function toStringJustification() : string
    {
        if ($this->getJustification() === null || $this->getJustification()==="") return "Aucune justification associée à la validation";
        return $this->getJustification();
    }

    public function toStringRefus() : string
    {
        return (!$this->isRefus()) ? "Validée" : "Refusée";
    }

    public function toStringValidateur() : string
    {
        return $this->getHistoCreateur()->getDisplayName();
    }
}