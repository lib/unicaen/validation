<?php

namespace UnicaenValidation\Entity;

use UnicaenValidation\Entity\Db\ValidationInstance;
use Doctrine\ORM\QueryBuilder;

interface HasValidationsInterface {

    public function getValidations() : array;
    public function addValidation(ValidationInstance $validation) : void;
    public function removeValidation(ValidationInstance $validation) : void;
    public function getValidationsByTypeCode(string $typeCode) : array ;
    public function getValidationActiveByTypeCode(string $typeCode) : ?ValidationInstance;

    static public function decorateWithValidations(QueryBuilder $qb, string $entityName,  array $validations = []) : QueryBuilder;
}