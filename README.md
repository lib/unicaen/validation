Bibliothèque unicaen/validation
===


La bibliothèque **unicaen/validation** est module proposant de gérer les validations déclaration des types de validation et mise en place des instances de validation. 

La classe *ValidationType* 
---

Un type de validation est constitué :
* d'un `code` unique permettant de récupérer simplement un type (idéalement regroupé dans un provider) ;
* d'un `libelle` décrivant le type de validation elle-même ; 
* d'un booléen `refusable` permettant si la validation est refusable (ce qui impact les formulaires de validation).

De plus les types sont munis de l'interface/trait `HistoriqueAware`.

La classe *ValidationInstance*
---

Une instance de validation est constitée :
* d'un `type` de validation (voir `ValidationType`) ;
* d'un booléen `refus` indiquant s'il s'agit d'une validation ou d'un refus de cette validation;
* d'une chaîne de caractère `justification` pour enregistrer la justification associé à cette validation.
    
L'interface *HasValidationsInterface* et le trait *HasValidationsTrait*  
---

Ajouter l'interface et le trait `HasValidations` va ajouter une collection `validations` (qu'il faut initialiser dans le constructeur) qui sera en charge de stocker les différentes validations.
`HasValidations` fournit les méthodes suivantes : 

```php
public function getValidations() : array;
public function addValidation(ValidationInstance $validation) : void;
public function removeValidation(ValidationInstance $validation) : void;
public function getValidationsByTypeCode(string $typeCode) : array ;
public function getValidationActiveByTypeCode(string $typeCode) : ?ValidationInstance;
```

Remarque :
___
Les instances de validation seront stockées dans la table `unicaen_validation_instance` et il est nécessaire de créer en linker `entite_validation`.

Changements
===

**6.0.1**
- Ajout d'une documentation

**6.0.0**
- Compatibilité PHP8

Scripts et description des tables
===
*Les scripts de création sont disponibles dans le répertoire `documentation`.*

Table `unicaen_validation_type` stocke la liste des types de validation. 

| Attribut              | Type          | Remarque                                                                             | 
|-----------------------|---------------|--------------------------------------------------------------------------------------| 
| id                    | serial        | clef primaire                                                                        |
| code                  | varchar(256)  | code unique facilitant la récupération d'un type donné                               |
| libelle               | varchar(1024) | libellé associé au type                                                              |
| refusable             | boolean       | boolean indiquant si la validation peut être refusé<br/> Joue sur les mises en forme |  
**Remarque :** L'entité associée est historisable et la table est munie des attributs associés.   


Table `unicaen_validation_instance` stocke la liste des types de validation.

| Attribut      | Type    | Remarque                                      | 
|---------------|---------|-----------------------------------------------| 
| id            | serial  | clef primaire                                 |
| type_id       | integer | clef étrangère vers `unicaen_validation_type` |
| refus         | boolean | Refusée ou non (default: `false`)             |
| justification | text    |                                               |  
**Remarque :** L'entité associée est historisable et la table est munie des attributs associés.

Troubleshooting
===

Aucun pour le moment ...