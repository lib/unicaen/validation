<?php

use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Laminas\Router\Http\Literal;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenValidation\Controller\ValidationInstanceController;
use UnicaenValidation\Controller\ValidationTypeController;

return [
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenValidation\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/UnicaenValidation/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'UNICAEN-VALIDATION__' . __NAMESPACE__,
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'validation' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/validation',
                ],
                'may_terminate' => false,
            ],
        ],
    ],

    
    'service_manager' => [
        'invokables' => [
            ],
        'factories' => [
        ],
    ],

    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'view_helpers'  => [
        'aliases' => [
        ],
        'factories' => [
        ],
    ],
];
